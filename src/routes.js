export default [
  {
    path: '/login/',
    component: require('./assets/vue/pages/login.vue')
  },
  {
    path: '/ip/',
    component: require('./assets/vue/pages/ip.vue')
  },
  {
    path: '/registration/',
    component: require('./assets/vue/pages/registration.vue')
  },
  {
    path: '/section/',
    component: require('./assets/vue/pages/section.vue')
  },
  {
    path: '/files/section_code/:sectionCode',
    component: require('./assets/vue/pages/files.vue')
  },


    {
    path: '/exams/section_code/:sectionCode',
    component: require('./assets/vue/pages/exams.vue')
  },
]
